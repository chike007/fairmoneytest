package com.chikemgbemena.fairmoneytest.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chikemgbemena.core.domain.ReactiveInteractor
import com.chikemgbemena.core.extensions.setError
import com.chikemgbemena.core.extensions.setLoading
import com.chikemgbemena.core.extensions.setSuccess
import com.chikemgbemena.core.networking.Scheduler
import com.chikemgbemena.core.presentation.Resource
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import com.chikemgbemena.fairmoneytest.di.GraphDH
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PostsViewModel @Inject constructor(
    private val postsUseCase: ReactiveInteractor.RetrieveInteractor<Boolean,
            List<PostEntity>>,
    private val appScheduler: Scheduler
) : ViewModel() {

    val liveData = MutableLiveData<Resource<List<PostEntity>>>()
    private val compositeDisposable = CompositeDisposable()

    fun get(refresh: Boolean = false) =
        compositeDisposable.add(postsUseCase.getBehaviorStream(refresh)
            .doOnSubscribe { liveData.setLoading() }
            .subscribeOn(appScheduler.io())
            .observeOn(appScheduler.mainThread())
            .subscribe({ liveData.setSuccess(it) }, { liveData.setError(it.message) })
        )

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
        GraphDH.destroyGraphComponent()
    }
}