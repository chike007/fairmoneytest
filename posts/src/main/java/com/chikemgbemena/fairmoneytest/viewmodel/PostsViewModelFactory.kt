package com.chikemgbemena.fairmoneytest.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chikemgbemena.core.domain.ReactiveInteractor
import com.chikemgbemena.core.networking.Scheduler
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity

@Suppress("UNCHECKED_CAST")
class PostsViewModelFactory(
    private val scheduler: Scheduler,
    private val useCase: ReactiveInteractor.RetrieveInteractor<Boolean,
            List<PostEntity>>
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostsViewModel(useCase, scheduler) as T
    }
}