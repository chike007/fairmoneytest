package com.chikemgbemena.fairmoneytest.ui

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chikemgbemena.fairmoneytest.R
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import kotlinx.android.synthetic.main.item_post.view.*

class PostsAdapter(private var posts: List<PostEntity>, private var activity: Activity) :
    RecyclerView.Adapter<PostsAdapter.PostViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val contentView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_post, parent, false)
        return PostViewHolder(contentView)
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    fun updatePosts(posts: List<PostEntity>) {
        this.posts = posts
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(posts[position])
    }

    inner class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(post: PostEntity) = with(itemView) {
            tv_title.text = post.title
            tv_body.text = context.getString(R.string.article_sub, post.body.substring(0, 50))
            cv_main.setOnClickListener {
                PostDetailActivity.start(activity, post)
            }
        }
    }
}