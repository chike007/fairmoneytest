package com.chikemgbemena.fairmoneytest.ui

import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.chikemgbemena.core.application.BaseActivity
import com.chikemgbemena.core.extensions.gone
import com.chikemgbemena.core.extensions.visible
import com.chikemgbemena.core.presentation.Resource
import com.chikemgbemena.core.presentation.ResourceState
import com.chikemgbemena.fairmoneytest.R
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import com.chikemgbemena.fairmoneytest.di.GraphDH
import com.chikemgbemena.fairmoneytest.viewmodel.PostsViewModel
import com.chikemgbemena.fairmoneytest.viewmodel.PostsViewModelFactory
import kotlinx.android.synthetic.main.activity_posts.*
import javax.inject.Inject

class PostsActivity : BaseActivity() {

    private val component by lazy { GraphDH.graphComponent() }
    @Inject
    lateinit var viewModelFactory: PostsViewModelFactory
    private val viewModel: PostsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostsViewModel::class.java)
    }

    private lateinit var postsAdapter: PostsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_posts)
        component.inject(this)

        viewModel.liveData.observe(this, Observer { onResult(it) })
        viewModel.get(false)

        swiperefresh.setOnRefreshListener {
            viewModel.get(true)
        }

        rv_posts.layoutManager = LinearLayoutManager(this)
        postsAdapter = PostsAdapter(mutableListOf(), this)
        rv_posts.adapter = postsAdapter
    }

    private fun onResult(resource: Resource<List<PostEntity>>?) {
        resource?.let { it ->
            when (it.state) {
                ResourceState.LOADING -> progressBar.visible()
                ResourceState.SUCCESS -> {
                    progressBar.gone()
                    swiperefresh.isRefreshing = false
                }
                ResourceState.ERROR -> {
                    progressBar.gone()
                    swiperefresh.isRefreshing = false
                }
            }
            it.data?.let {
                postsAdapter.updatePosts(it)
            }
            it.message?.let { Toast.makeText(this, it, Toast.LENGTH_LONG).show() }
        }
    }
}
