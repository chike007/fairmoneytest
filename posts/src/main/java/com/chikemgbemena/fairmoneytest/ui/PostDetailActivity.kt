package com.chikemgbemena.fairmoneytest.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.chikemgbemena.core.application.BaseActivity
import com.chikemgbemena.fairmoneytest.R
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import kotlinx.android.synthetic.main.activity_post_detail.*

const val EXTRA_POST_ENTITY = "EXTRA_POST_ENTITY"

class PostDetailActivity : BaseActivity() {

    companion object {
        fun start(caller: Activity, postEntity: PostEntity) {
            val intent = Intent(caller, PostDetailActivity::class.java)
            intent.putExtra(EXTRA_POST_ENTITY, postEntity)
            caller.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_detail)

        intent.getParcelableExtra<PostEntity>(EXTRA_POST_ENTITY)?.let {
            tv_title.text = it.title
            tv_body.text = it.body
        }
    }
}
