package  com.chikemgbemena.fairmoneytest.data.source.remote

import com.chikemgbemena.fairmoneytest.data.source.local.model.PostData
import io.reactivex.Single
import retrofit2.http.GET

interface PostsService {

    @GET("posts")
    fun getPosts(): Single<List<PostData>>
}