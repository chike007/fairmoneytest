package com.chikemgbemena.fairmoneytest.data.datasource

import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import io.reactivex.Single

interface PostsDataSourceContract {

    interface Repository {
        fun getPosts(refresh: Boolean): Single<List<PostEntity>>
    }

    interface Local {
        fun getPosts(): Single<List<PostEntity>>
        fun savePosts(posts: List<PostEntity>): Single<List<PostEntity>>
    }

    interface Remote {
        fun getPosts(): Single<List<PostEntity>>
    }
}