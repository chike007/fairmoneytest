package com.chikemgbemena.fairmoneytest.data

import com.chikemgbemena.fairmoneytest.data.datasource.PostsDataSourceContract
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import io.reactivex.Single

class PostsRepository(
    private val local: PostsDataSourceContract.Local,
    private val remote: PostsDataSourceContract.Remote
) : PostsDataSourceContract.Repository {

    override fun getPosts(refresh: Boolean): Single<List<PostEntity>> =
        when (refresh) {
            true -> remote.getPosts().flatMap {
                local.savePosts(it)
            }
            false -> local.getPosts().onErrorResumeNext {
                getPosts(true)
            }
        }
}