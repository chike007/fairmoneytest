package com.chikemgbemena.fairmoneytest.data.datasource

import com.chikemgbemena.core.domain.Mapper
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import com.chikemgbemena.fairmoneytest.data.source.local.model.PostData
import com.chikemgbemena.fairmoneytest.data.source.remote.PostsService
import io.reactivex.Single

class PostsRemoteDataSource(
    private val service: PostsService,
    private val dataMapper: Mapper<PostData, PostEntity>
) : PostsDataSourceContract.Remote {

    override fun getPosts(): Single<List<PostEntity>> {
        return service.getPosts().map { result ->
            result.map { dataMapper.mapFrom(it) }
        }
    }
}