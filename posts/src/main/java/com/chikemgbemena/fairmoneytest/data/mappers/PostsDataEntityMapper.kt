package com.chikemgbemena.fairmoneytest.data.mappers

import com.chikemgbemena.core.domain.Mapper
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import com.chikemgbemena.fairmoneytest.data.source.local.model.PostData
import javax.inject.Inject

class PostsDataEntityMapper @Inject constructor() : Mapper<PostData, PostEntity>() {
    override fun mapFrom(from: PostData): PostEntity {
        return PostEntity(
            id = from.id,
            title = from.title,
            body = from.body
        )
    }
}
