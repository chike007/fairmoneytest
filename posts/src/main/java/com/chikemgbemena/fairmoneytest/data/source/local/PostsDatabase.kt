package com.chikemgbemena.fairmoneytest.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.chikemgbemena.fairmoneytest.data.source.local.dao.PostsDao
import com.chikemgbemena.fairmoneytest.data.source.local.model.PostData

@Database(entities = [PostData::class], version = 1, exportSchema = false)
abstract class PostsDatabase : RoomDatabase() {

    abstract val postsDao: PostsDao

    companion object {
        const val DB_NAME = "fairmoneytest.db"
    }
}
