package com.chikemgbemena.fairmoneytest.data.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostEntity (
    val id: Int = 0,
    val userId: Int = 0,
    val title: String,
    val body: String
) : Parcelable