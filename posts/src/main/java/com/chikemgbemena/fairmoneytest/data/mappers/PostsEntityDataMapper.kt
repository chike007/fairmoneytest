package com.chikemgbemena.fairmoneytest.data.mappers

import com.chikemgbemena.core.domain.Mapper
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import com.chikemgbemena.fairmoneytest.data.source.local.model.PostData
import javax.inject.Inject

class PostsEntityDataMapper @Inject
constructor() : Mapper<PostEntity, PostData>() {
    override fun mapFrom(from: PostEntity): PostData {
        return PostData(
            id = from.id,
            userId = from.userId,
            title = from.title,
            body = from.body
        )
    }
}