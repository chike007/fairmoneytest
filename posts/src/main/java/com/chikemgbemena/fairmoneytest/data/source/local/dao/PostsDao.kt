package com.chikemgbemena.fairmoneytest.data.source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chikemgbemena.fairmoneytest.data.source.local.model.PostData

@Dao
interface PostsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(postData: List<PostData>): List<Long>

    @Query("SELECT * FROM posts")
    fun fetchAll(): List<PostData>

    @Query("DELETE FROM posts")
    fun deleteAll()
}