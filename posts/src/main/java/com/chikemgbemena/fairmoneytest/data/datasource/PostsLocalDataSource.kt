package com.chikemgbemena.fairmoneytest.data.datasource

import com.chikemgbemena.core.domain.Mapper
import com.chikemgbemena.core.extensions.performOnBack
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import com.chikemgbemena.fairmoneytest.data.source.local.PostsDatabase
import com.chikemgbemena.fairmoneytest.data.source.local.model.PostData
import io.reactivex.Completable
import io.reactivex.Single

class PostsLocalDataSource(
    private val db: PostsDatabase,
    private val scheduler: com.chikemgbemena.core.networking.Scheduler,
    private val dataEntityMapper: Mapper<PostData, PostEntity>,
    private val entityDataMapper: Mapper<PostEntity, PostData>
) : PostsDataSourceContract.Local {

    override fun getPosts(): Single<List<PostEntity>> {
        return Single.fromCallable {
            val result = db.postsDao.fetchAll()
            if (result.isNotEmpty()) {
                result.map {
                    dataEntityMapper.mapFrom(it)
                }
            } else {
                throw Exception("Empty values")
            }
        }
    }

    override fun savePosts(posts: List<PostEntity>): Single<List<PostEntity>> {
        Completable.fromCallable {
            val dao = db.postsDao
            dao.deleteAll()
            dao.insert(posts.map { entityDataMapper.mapFrom(it) })
        }
            .performOnBack(scheduler)
            .subscribe()
        return Single.just(posts)
    }
}