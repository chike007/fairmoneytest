package com.chikemgbemena.fairmoneytest.usecase

import com.chikemgbemena.core.domain.ReactiveInteractor
import com.chikemgbemena.fairmoneytest.data.datasource.PostsDataSourceContract
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import io.reactivex.Observable
import javax.inject.Inject

class RetrievePostsUseCase @Inject constructor(
    private val repository: PostsDataSourceContract.Repository
) : ReactiveInteractor.RetrieveInteractor<Boolean, List<PostEntity>> {

    override fun getBehaviorStream(params: Boolean): Observable<List<PostEntity>> {
        return repository.getPosts(params).toObservable()
    }
}