package com.chikemgbemena.fairmoneytest.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PostsScope