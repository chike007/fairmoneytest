package com.chikemgbemena.fairmoneytest.di

import android.content.Context
import androidx.room.Room
import com.chikemgbemena.core.di.CoreComponent
import com.chikemgbemena.core.domain.ReactiveInteractor
import com.chikemgbemena.core.networking.Scheduler
import com.chikemgbemena.fairmoneytest.data.PostsRepository
import com.chikemgbemena.fairmoneytest.data.datasource.PostsDataSourceContract
import com.chikemgbemena.fairmoneytest.data.datasource.PostsLocalDataSource
import com.chikemgbemena.fairmoneytest.data.datasource.PostsRemoteDataSource
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import com.chikemgbemena.fairmoneytest.data.mappers.PostsDataEntityMapper
import com.chikemgbemena.fairmoneytest.data.mappers.PostsEntityDataMapper
import com.chikemgbemena.fairmoneytest.data.source.local.PostsDatabase
import com.chikemgbemena.fairmoneytest.data.source.local.dao.PostsDao
import com.chikemgbemena.fairmoneytest.data.source.remote.PostsService
import com.chikemgbemena.fairmoneytest.ui.PostsActivity
import com.chikemgbemena.fairmoneytest.usecase.RetrievePostsUseCase
import com.chikemgbemena.fairmoneytest.viewmodel.PostsViewModelFactory
import dagger.Component
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit

@PostsScope
@Component(dependencies = [CoreComponent::class], modules = [PostsModule::class])
interface GraphComponent {
    fun inject(graphActivity: PostsActivity)
}

@Module
class PostsModule {

    @Provides
    @PostsScope
    fun providePostsViewModelFactory(
        scheduler: Scheduler,
        useCase: ReactiveInteractor.RetrieveInteractor<Boolean,
                List<PostEntity>>
    ): PostsViewModelFactory = PostsViewModelFactory(scheduler, useCase)

    @Provides
    @PostsScope
    fun providePostsRepo(
        local: PostsDataSourceContract.Local, remote: PostsDataSourceContract.Remote
    ): PostsDataSourceContract.Repository = PostsRepository(local, remote)

    @Provides
    @PostsScope
    internal fun provideAppDatabase(context: Context): PostsDatabase {
        return Room.databaseBuilder(
            context,
            PostsDatabase::class.java,
            PostsDatabase.DB_NAME
        ).allowMainThreadQueries().build()
    }

    @Provides
    @PostsScope
    internal fun providePostsDao(appDatabase: PostsDatabase): PostsDao = appDatabase.postsDao

    @Provides
    @PostsScope
    fun provideRemoteData(
        postsService: PostsService,
        dataMapper: PostsDataEntityMapper
    ): PostsDataSourceContract.Remote = PostsRemoteDataSource(postsService, dataMapper)

    @Provides
    @PostsScope
    fun provideLocalData(
        db: PostsDatabase, scheduler: Scheduler,
        dataEntityMapper: PostsDataEntityMapper,
        entityDataMapper: PostsEntityDataMapper
    ): PostsDataSourceContract.Local = PostsLocalDataSource(db, scheduler, dataEntityMapper,
        entityDataMapper)

    @Provides
    @PostsScope
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @PostsScope
    fun provideRetrievePostsUseCase(repo: PostsDataSourceContract.Repository):
            ReactiveInteractor.RetrieveInteractor<Boolean, List<PostEntity>> =
        RetrievePostsUseCase(repo)

    @Provides
    @PostsScope
    fun providePostsService(retrofit: Retrofit): PostsService = retrofit.create(PostsService::class.java)
}