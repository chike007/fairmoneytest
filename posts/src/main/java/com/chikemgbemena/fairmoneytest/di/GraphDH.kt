package com.chikemgbemena.fairmoneytest.di

import com.chikemgbemena.core.application.Application
import javax.inject.Singleton

@Singleton
object GraphDH {

    private var graphComponent: GraphComponent? = null

    fun graphComponent(): GraphComponent {
        if (graphComponent == null)
            graphComponent =
                DaggerGraphComponent.builder().coreComponent(Application.coreComponent).build()
        return graphComponent as GraphComponent
    }

    fun destroyGraphComponent() {
        graphComponent = null
    }
}