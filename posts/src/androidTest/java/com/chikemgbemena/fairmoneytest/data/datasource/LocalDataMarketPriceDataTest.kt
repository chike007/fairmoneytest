package com.chikemgbemena.fairmoneytest.data.datasource

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.chikemgbemena.fairmoneytest.TestScheduler
import com.chikemgbemena.fairmoneytest.data.mappers.PostsDataEntityMapper
import com.chikemgbemena.fairmoneytest.data.mappers.PostsEntityDataMapper
import com.chikemgbemena.fairmoneytest.data.source.local.PostsDatabase
import com.chikemgbemena.fairmoneytest.data.source.local.dao.MarketPriceDao
import com.chikemgbemena.fairmoneytest.testing.DummyData
import junit.framework.Assert.assertEquals
import org.junit.*
import org.junit.runner.RunWith
import java.io.IOException

/**
 * Tests for [PostsLocalDataSource]
 **/
@RunWith(AndroidJUnit4::class)
class LocalDataMarketPriceDataTest {

    private lateinit var postsDb: PostsDatabase
    private lateinit var marketPriceDao: MarketPriceDao
    private val dataEntityMapper = PostsDataEntityMapper()
    private val entityDataMapper = PostsEntityDataMapper()

    private val localDataSource: PostsLocalDataSource by lazy {
        PostsLocalDataSource(
            postsDb,
            TestScheduler(),
            dataEntityMapper,
            entityDataMapper
        )
    }

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val dummyMarketPrices = listOf(
        DummyData.marketPriceData(
            1,
            1572307200, 9385.6F
        ), DummyData.marketPriceData(
            2,
            1572393600, 9205.501666666669F
        )
    )

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        postsDb = Room
            .inMemoryDatabaseBuilder(context, PostsDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        marketPriceDao = postsDb.marketPriceDao
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        postsDb.close()
    }

    @Test
    fun testGetMarketPrices() {
        marketPriceDao.insert(dummyMarketPrices)
        val entities = dummyMarketPrices.map {
            dataEntityMapper.mapFrom(it)
        }
        localDataSource.getMarketPrice().test().run {
            assertEquals(values()[0], entities)
        }
    }

    @Test
    fun testSaveMarketPrices() {
        localDataSource.saveMarketPrice(dummyMarketPrices.map {
            dataEntityMapper.mapFrom(it)
        })
        val marketPrices = marketPriceDao.fetchAll()
        Assert.assertEquals(marketPrices, dummyMarketPrices)
    }
}