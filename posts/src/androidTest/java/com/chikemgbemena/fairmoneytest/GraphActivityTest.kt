package com.chikemgbemena.fairmoneytest

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.chikemgbemena.fairmoneytest.ui.PostsActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class GraphActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(
        PostsActivity::class.java, false,
        false)

    @Test
    fun appLaunchesSuccessfully() {
        ActivityScenario.launch(PostsActivity::class.java)
    }

    @Test
    fun onLaunchCheckViewsDisplayed() {
        ActivityScenario.launch(PostsActivity::class.java)

        onView(withId(R.id.tvTitle)).check(matches(isDisplayed()))
        onView(withId(R.id.tvSubMessage)).check(matches(isDisplayed()))
        onView(withId(R.id.tvSource)).check(matches(isDisplayed()))
        onView(withId(R.id.chart)).check(matches(isDisplayed()))
    }
}