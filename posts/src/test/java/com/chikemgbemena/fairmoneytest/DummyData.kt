package com.chikemgbemena.fairmoneytest

import androidx.annotation.VisibleForTesting
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import com.chikemgbemena.fairmoneytest.data.source.local.model.PostData

@VisibleForTesting(otherwise = VisibleForTesting.NONE)
object DummyData {

    fun getPosts(): List<PostData> {
        val posts = mutableListOf<PostData>()
        val postOne = PostData(0, 2, "title 1", "body 2")
        posts.add(postOne)
        val postTwo = PostData(1, 2, "title 2", "body 2")
        posts.add(postTwo)
        val postThree = PostData(3, 4, "title 3", "body 3")
        posts.add(postThree)
        return posts
    }

    fun postEntity(id: Int, userId: Int, title: String, body: String) =
        PostEntity(id, userId, title, body)

}