package com.chikemgbemena.fairmoneytest.usecase

import com.chikemgbemena.fairmoneytest.data.datasource.PostsDataSourceContract
import com.chikemgbemena.fairmoneytest.DummyData
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class RetrievePostsUseCaseTest {

    private lateinit var graphUseCase: RetrievePostsUseCase

    private val mockRepository: PostsDataSourceContract.Repository = mock()
    private val entities = DummyData.postEntity(1, 343, "title 333", "body 333")

    companion object {
        const val refresh = true
        const val timeSpan = "30days"
    }

    @Before
    fun `set up`() {
        graphUseCase = RetrievePostsUseCase(mockRepository)
    }

    @Test
    fun `get post list from repository get success`() {
        whenever(mockRepository.getPosts(refresh))
            .thenReturn(Single.just(listOf(entities)))

        val test = graphUseCase.getBehaviorStream(refresh).test()

        verify(mockRepository).getPosts(refresh)

        test.assertNoErrors()
        test.assertComplete()
        test.assertValueCount(1)
        test.assertValue(listOf(entities))
    }

    @Test
    fun `get post list from repository get fail`() {
        val throwable = Throwable()
        whenever(mockRepository.getPosts(refresh)).thenReturn(Single.error(throwable))

        val test = graphUseCase.getBehaviorStream(refresh).test()

        verify(mockRepository).getPosts(refresh)

        test.assertNoValues()
        test.assertNotComplete()
        test.assertError(throwable)
        test.assertValueCount(0)
    }
}