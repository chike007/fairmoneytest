package com.chikemgbemena.fairmoneytest.viewmodel.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chikemgbemena.core.domain.ReactiveInteractor
import com.chikemgbemena.core.presentation.Resource
import com.chikemgbemena.core.presentation.ResourceState
import com.chikemgbemena.fairmoneytest.TestScheduler
import com.chikemgbemena.fairmoneytest.DummyData
import com.chikemgbemena.fairmoneytest.data.entity.PostEntity
import com.chikemgbemena.fairmoneytest.viewmodel.PostsViewModel
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class PostsViewModelTest {

    private lateinit var viewModel: PostsViewModel
    private val mockUseCase: ReactiveInteractor.RetrieveInteractor<Boolean, List<PostEntity>> = mock()
    private val throwable = Throwable()

    @Rule
    @JvmField
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    companion object {
        const val refresh = true
        val marketPriceEntities = listOf(
            DummyData.postEntity(1, 1572307200, "title", "body"))
    }

    @Before
    fun `set up`() {
        viewModel = PostsViewModel(
            mockUseCase,
            TestScheduler()
        )
    }

    @Test
    fun `get post list success`() {
        whenever(mockUseCase.getBehaviorStream(refresh))
            .thenReturn(Observable.just(marketPriceEntities))

        viewModel.get(refresh)

        verify(mockUseCase).getBehaviorStream(refresh)
        Assert.assertEquals(
            Resource(ResourceState.SUCCESS, marketPriceEntities, null),
            viewModel.liveData.value
        )
    }

    @Test
    fun `get posts list success fails`() {
        whenever(mockUseCase.getBehaviorStream(refresh))
            .thenReturn(Observable.error(throwable))

        viewModel.get(refresh)

        verify(mockUseCase).getBehaviorStream(refresh)
        Assert.assertEquals(
            Resource(state = ResourceState.ERROR, data = null, message = throwable.message),
            viewModel.liveData.value
        )
    }
}