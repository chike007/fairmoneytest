package com.chikemgbemena.fairmoneytest.data.datasource

import com.chikemgbemena.fairmoneytest.data.mappers.PostsDataEntityMapper
import com.chikemgbemena.fairmoneytest.data.source.remote.PostsService
import com.chikemgbemena.fairmoneytest.DummyData
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Assert
import org.junit.Test

/**
 * Tests for [PostsRemoteDataSource]
 */
class RemoteDataMarketPriceDataTest {

    private val marketPriceService = mock<PostsService>()
    private val dataMapper = PostsDataEntityMapper()
    private val throwable = Throwable()

    private var postsRemoteDataSource: PostsRemoteDataSource

    init {
        postsRemoteDataSource = PostsRemoteDataSource(
            marketPriceService, dataMapper
        )
    }

    @Test
    fun `get posts success`() {
        whenever(marketPriceService.getPosts()).thenReturn(
            Single.just(DummyData.getPosts())
        )
        postsRemoteDataSource.getPosts()
            .test().run {
                assertNoErrors()
                assertValueCount(1)
                Assert.assertEquals(values()[0].size, 3)
                Assert.assertEquals(values()[0][0].title, "title 1")
                Assert.assertEquals(values()[0][0].body, "body 2")
            }
    }

    @Test
    fun `get posts failure`() {
        whenever(marketPriceService.getPosts()).thenReturn(
            Single.error(throwable)
        )

        postsRemoteDataSource.getPosts()
            .test()
            .run {
                assertError(throwable)
            }
    }
}