package com.chikemgbemena.fairmoneytest.data.source.remote

import com.chikemgbemena.fairmoneytest.DependencyProvider
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

/**
 * Tests for [PostsService]
 */
class RemoteDataPostsDataTest {

    private lateinit var service: PostsService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun init() {
        mockWebServer = MockWebServer()
        service = DependencyProvider
            .getRetrofit(mockWebServer.url("/"))
            .create(PostsService::class.java)
    }

    @After
    @Throws(IOException::class)
    fun `tear down`() {
        mockWebServer.shutdown()
    }

    @Test
    fun `get posts 200`() {
        queueResponse {
            setResponseCode(200)
            setBody(DependencyProvider.getResponseFromJson("posts"))
        }

        service
            .getPosts()
            .test()
            .run {
                assertNoErrors()
                assertValueCount(1)
                Assert.assertEquals(values()[0].size, 100)
                Assert.assertEquals(values()[0][0].id, 1)
            }
    }

    private fun queueResponse(block: MockResponse.() -> Unit) {
        mockWebServer.enqueue(MockResponse().apply(block))
    }
}