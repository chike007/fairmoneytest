package com.chikemgbemena.fairmoneytest.data

import com.chikemgbemena.fairmoneytest.data.datasource.PostsDataSourceContract
import com.chikemgbemena.fairmoneytest.DummyData
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class PostsRepositoryTest {

    private lateinit var repository: PostsRepository

    private val mockLocalPostsDataSourceSource: PostsDataSourceContract.Local = mock()
    private val mockRemotePostsDataSourceSource: PostsDataSourceContract.Remote = mock()

    private val localItem = DummyData.postEntity(1, 1572307200, "title 1",
    "body 1")
    private val remoteItem = DummyData.postEntity(66, 1572307200, "title 343",
    "body 343234")

    private val localList = listOf(localItem)
    private val remoteList = listOf(remoteItem)

    private val localThrowable = Throwable()
    private val remoteThrowable = Throwable()

    @Before
    fun `set up`() {
        repository = PostsRepository(mockLocalPostsDataSourceSource, mockRemotePostsDataSourceSource)
    }

    @Test
    fun `get post list from local success`() {
        whenever(mockLocalPostsDataSourceSource.getPosts()).thenReturn(Single.just(localList))
        val test = repository.getPosts(false).test()
        verify(mockLocalPostsDataSourceSource).getPosts()
        test.assertValue(localList)
    }

    @Test
    fun `get post list from local fail fallback remote succeeds`() {
        whenever(mockLocalPostsDataSourceSource.getPosts()).thenReturn(Single.error(localThrowable))
        whenever(mockRemotePostsDataSourceSource.getPosts()).thenReturn(Single.just(remoteList))
        whenever(mockLocalPostsDataSourceSource.savePosts(remoteList)).thenReturn(Single.just(remoteList))

        val test = repository.getPosts(false).test()

        verify(mockLocalPostsDataSourceSource).getPosts()
        verify(mockRemotePostsDataSourceSource).getPosts()
        verify(mockLocalPostsDataSourceSource).savePosts(remoteList)
        test.assertValue(remoteList)
    }

    @Test
    fun `get post list from local fail fallback remote fails`() {
        whenever(mockLocalPostsDataSourceSource.getPosts()).thenReturn(Single.error(localThrowable))
        whenever(mockRemotePostsDataSourceSource.getPosts()).thenReturn(Single.error(remoteThrowable))

        val test = repository.getPosts(false).test()

        verify(mockLocalPostsDataSourceSource).getPosts()
        verify(mockRemotePostsDataSourceSource).getPosts()
        test.assertError(remoteThrowable)
    }
}