package com.chikemgbemena.core.di.modules

import android.content.Context
import com.chikemgbemena.core.networking.AppScheduler
import com.chikemgbemena.core.networking.Scheduler
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class ApplicationModule(private val context: Context) {

    @Provides
    @Reusable
    fun provideContext(): Context {
        return context
    }

    @Provides
    @Reusable
    fun provideAppScheduler(): Scheduler = AppScheduler()
}
