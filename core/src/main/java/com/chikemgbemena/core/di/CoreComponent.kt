package com.chikemgbemena.core.di

import android.content.Context
import com.chikemgbemena.core.di.modules.ApplicationModule
import com.chikemgbemena.core.di.modules.NetworkModule
import com.chikemgbemena.core.networking.Scheduler
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class])
interface CoreComponent {

    fun scheduler(): Scheduler
    fun context(): Context
    fun retrofit(): Retrofit
}