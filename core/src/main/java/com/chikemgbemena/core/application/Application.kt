package com.chikemgbemena.core.application

import android.app.Application
import com.chikemgbemena.core.di.CoreComponent
import com.chikemgbemena.core.di.DaggerCoreComponent
import com.chikemgbemena.core.di.modules.ApplicationModule
import com.chikemgbemena.core.di.modules.NetworkModule

class Application : Application() {

    companion object {
        lateinit var coreComponent: CoreComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDI()
    }

    private fun initDI() {
        coreComponent = DaggerCoreComponent.builder()
            .applicationModule(ApplicationModule(this))
            .networkModule(NetworkModule).build()
    }
}
