package com.chikemgbemena.core.extensions

import io.reactivex.Completable

fun Completable.performOnBack(scheduler: com.chikemgbemena.core.networking.Scheduler): Completable {
    return this.subscribeOn(scheduler.io())
}