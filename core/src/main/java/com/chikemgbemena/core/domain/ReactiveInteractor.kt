package com.chikemgbemena.core.domain

import androidx.annotation.NonNull
import io.reactivex.Flowable
import io.reactivex.Observable

/**
 * Interfaces for Interactors. This interfaces represent use cases (this means any use case in the application should implement this contract).
 * https://github.com/n26/N26AndroidSamples/blob/master/base/src/main/java/de/n26/n26androidsamples/base/domain/ReactiveInteractor.java
 */
interface ReactiveInteractor {

    /**
     * Retrieves changes from the data layer.
     * It returns an [Flowable] that emits updates for the retrieved object. The returned [Flowable] will never complete, but it can
     * error if there are any problems performing the required actions to serve the data.
     *
     * @param <Object> the type of the retrieved object.
     * @param <Params> required parameters for the retrieve operation.
    </Params></Object> */
    interface RetrieveInteractor<Params, Object> : ReactiveInteractor {

        @NonNull
        fun getBehaviorStream(@NonNull params: Params): Observable<Object>
    }
}

